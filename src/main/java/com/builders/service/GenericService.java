package com.builders.service;



import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;


public abstract interface GenericService<T> {
	
	public Page<T> getAll(Pageable pageable);	
	
	public Collection<T> getAll();
	
	public List<T> getAll(Specification<T> spec);
	
	public T save(T entity);
	
	public void remove(T entity);
	
	public Optional<T> getId(Integer id);
	

}