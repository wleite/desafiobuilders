package com.builders.controller;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.builders.exception.ResourceNotFoundException;
import com.builders.filter.ClienteSpecificationBuilder;
import com.builders.model.Cliente;
import com.builders.service.GenericService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
@RequestMapping("cliente")
@Api(value = "Api Cadastro de Clientes")
public class ClienteController {

	@Autowired
	private GenericService<Cliente> service;
	
	@ApiOperation(value = "Consultar todos os Clientes")
	@GetMapping("/")
	public ResponseEntity<Page<Cliente>> getAll(Pageable pageable){
		Page<Cliente> clientes = service.getAll(pageable);		
		if(clientes.isEmpty()) {
        	return new ResponseEntity<Page<Cliente>>(HttpStatus.NO_CONTENT);
        }   
		return new ResponseEntity<>(clientes,HttpStatus.OK);
	}	
	
	@ApiOperation(value = "Consultar Clientes por filtro")
	@GetMapping("/search")
	 public ResponseEntity<List<Cliente>> search(@RequestParam(value = "search") String search) {
        ClienteSpecificationBuilder builder = new ClienteSpecificationBuilder();
        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
        Matcher matcher = pattern.matcher(search + ",");
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
        }                
        
        Specification<Cliente> spec = builder.build();
        
        List<Cliente> clientes = service.getAll(spec);
        if(clientes.isEmpty()) {
        	return new ResponseEntity<List<Cliente>>(HttpStatus.NO_CONTENT);
        }        
        return new ResponseEntity<>(clientes,HttpStatus.OK);
    }
	
	@ApiOperation(value = "Consultar Cliente por Id")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Cliente> getObject(@PathVariable("id") Integer id) throws ResourceNotFoundException {
		Cliente cliente = service.getId(id)
           .orElseThrow(() -> new ResourceNotFoundException("Cliente não encontrado: " + id));
        return new ResponseEntity<Cliente>(cliente,HttpStatus.OK);
	}
		
	@ApiOperation(value = "Inserir Cliente")
	@PostMapping(value="/")
	public ResponseEntity<Cliente> create(@RequestBody Cliente cliente){
		cliente.setCreated(new Date());
		service.save(cliente);		
		return new ResponseEntity<Cliente>(HttpStatus.CREATED);
	}	
	
	@ApiOperation(value = "Atualizar Cliente")
    @PutMapping(value="/")
	public ResponseEntity<Cliente> update(@RequestBody Cliente cliente)throws ResourceNotFoundException{	
		Cliente item = service.getId(cliente.getId())
		           .orElseThrow(() -> new ResourceNotFoundException("Cliente não encontrado: " + cliente.getId()));
		cliente.setUpdated(new Date());
		item = service.save(cliente);		
		return new ResponseEntity<Cliente>(item,HttpStatus.ACCEPTED);
	}	

	@ApiOperation(value = "Excluir Cliente por Id")
 	@DeleteMapping(value="/{id}")
	public ResponseEntity<Collection<Cliente>> delete(@PathVariable Integer id)throws ResourceNotFoundException{
		Cliente cliente = service.getId(id)
		           .orElseThrow(() -> new ResourceNotFoundException("Cliente não encontrado: " + id));

		service.remove(cliente);
		return new ResponseEntity<>(HttpStatus.OK);
	}		
}
