package com.builders.util;

import java.util.Date;

import com.builders.model.Cliente;

public class ClienteCreator {

	public static Cliente createClienteToBeSaved() {		
		Cliente cliente = new Cliente();
		cliente.setNome("William");
		cliente.setSobrenome("Ferreira Leite");
		cliente.setCpf("12345678914");
		cliente.setLogradouro("Rua 123");
		cliente.setNumero("123");
		cliente.setComplemento("casa 01");
		cliente.setBairro("Centro");
		cliente.setCidade("Rio de Janeiro");
		cliente.setUf("RJ");
		cliente.setCep("20745000");
		cliente.setDataNascimento(new Date("12/02/1982"));		
		return cliente;				          
	}
	
	public static Cliente createClienteValid() {		
		Cliente cliente = new Cliente();
		cliente.setNome("William");
		cliente.setSobrenome("Ferreira Leite");
		cliente.setCpf("12345678913");
		cliente.setLogradouro("Rua 123");
		cliente.setNumero("123");
		cliente.setComplemento("casa 01");
		cliente.setBairro("Centro");
		cliente.setCidade("Rio de Janeiro");
		cliente.setUf("RJ");
		cliente.setCep("20745000");
		cliente.setDataNascimento(new Date("12/02/1982"));		
		return cliente;			 
					          
	}
	
	public static Cliente createClienteValid1() {		
		Cliente cliente = new Cliente();
		cliente.setId(2);
		cliente.setNome("William");
		cliente.setSobrenome("Ferreira Leite");
		cliente.setCpf("12345678913");
		cliente.setLogradouro("Rua 123");
		cliente.setNumero("123");
		cliente.setComplemento("casa 01");
		cliente.setBairro("Centro");
		cliente.setCidade("Rio de Janeiro");
		cliente.setUf("RJ");
		cliente.setCep("20745000");
		cliente.setDataNascimento(new Date("12/02/1982"));		
		return cliente;			 
					          
	}
	
	public static Cliente createValidUpdateClienteToBeSaved() {
		Cliente cliente = new Cliente();
		cliente.setId(1);
		cliente.setNome("William");
		cliente.setSobrenome("Ferreira Leite");
		cliente.setCpf("10987061611");
		cliente.setLogradouro("Rua 4");
		cliente.setNumero("356");
		cliente.setComplemento("casa 02");
		cliente.setBairro("Centro");
		cliente.setCidade("Rio de Janeiro");
		cliente.setUf("RJ");
		cliente.setCep("20745000");
		cliente.setDataNascimento(new Date("12/02/1982"));
		cliente.setCreated(new Date());
		return cliente;			
	}
	
	public static Cliente ClienteToBeSearch() {
		Cliente cliente = new Cliente();
		cliente.setId(1);
		cliente.setNome("Fernando");
		cliente.setSobrenome("Leite");
		cliente.setCpf("10987061612");
		cliente.setLogradouro("Rua 4");
		cliente.setNumero("356");
		cliente.setDataNascimento(new Date("12/02/1982"));
		cliente.setCep("20745000");
		cliente.setComplemento("casa 02");
		cliente.setBairro("Centro");
		cliente.setCidade("Rio de Janeiro");
		cliente.setUf("RJ");
		cliente.setCreated(new Date());
		return cliente;			
	}
	
	public static Cliente createCliente2ToBeSaved() {		
		Cliente cliente = new Cliente();
		cliente.setNome("Fernando");
		cliente.setSobrenome("Abreu Leite");
		cliente.setCpf("12121212146");
		cliente.setLogradouro("Rua 123");
		cliente.setNumero("123");
		cliente.setComplemento("casa 01");
		cliente.setBairro("Centro");
		cliente.setCidade("Rio de Janeiro");
		cliente.setUf("RJ");
		cliente.setCep("20745000");
		cliente.setDataNascimento(new Date("28/10/2010"));		
		return cliente;				          
	}
	
}
