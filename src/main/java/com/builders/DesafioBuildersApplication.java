package com.builders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@ComponentScan(basePackages={"com.builders.*"})
@EntityScan(basePackages ={"com.builders.model"})
@EnableJpaRepositories(basePackages ={"com.builders.repository"})
@SpringBootApplication
@EnableSwagger2
public class DesafioBuildersApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioBuildersApplication.class, args);
	}
}