package com.builders.serviceimp;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.builders.model.Cliente;
import com.builders.repository.ClienteRepository;
import com.builders.service.GenericService;

@Service
@Transactional
public class ClienteServiceImp implements GenericService<Cliente>{

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public Page<Cliente> getAll(Pageable pageable) {
		return clienteRepository.findAll(pageable);
	}

	@Override
	public Collection<Cliente> getAll() {
 		return clienteRepository.findAll();
	}

	@Override
	public Cliente save(Cliente entity) {
		return clienteRepository.save(entity);
	}

	@Override
	public void remove(Cliente entity) {
       clienteRepository.delete(entity);		
	}

	@Override
	public Optional<Cliente> getId(Integer id) {
		return clienteRepository.findById(id);
	}

	@Override
	public List<Cliente> getAll(Specification<Cliente> spec) {
		return clienteRepository.findAll(spec);
	}
	
}
	