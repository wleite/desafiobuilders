package com.builders.service.test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.builders.model.Cliente;
import com.builders.repository.ClienteRepository;
import com.builders.service.GenericService;
import com.builders.util.ClienteCreator;

@SpringBootTest
public class ClienteServiceTest {

	@Autowired
    private GenericService<Cliente> service;
	
	@MockBean
	private ClienteRepository repository;
	
	
	@SuppressWarnings("deprecation")
	@Test
    @DisplayName("Test findAll Page")
    void FindAllPage() {
        // Setup our mock repository
		
		List<Cliente>lista = new ArrayList<Cliente>();
		Cliente cliente1 = ClienteCreator.createClienteToBeSaved();
		Cliente cliente2= ClienteCreator.createClienteValid();
		
		lista.add(cliente1);
		lista.add(cliente2);
		
		Page<Cliente> pagedTasks = new PageImpl<Cliente>(lista);
		
        doReturn(pagedTasks).when(repository).findAll(org.mockito.Matchers.isA(Pageable.class));
        
        Pageable pg = PageRequest.of(0,2);
        // Execute the service call
        Collection<Cliente> clientes = service.getAll(pg).getContent();

        // Assert the response
        Assertions.assertEquals(lista.size(), clientes.size(), "findAll should return 2 clientes");
    }
	
	@Test
    @DisplayName("Test findAll")
    void testFindAll() {
        // Setup our mock repository
		Cliente cliente1 = ClienteCreator.createClienteToBeSaved();
		Cliente cliente2= ClienteCreator.createClienteValid();
        doReturn(Arrays.asList(cliente1, cliente2)).when(repository).findAll();

        // Execute the service call
        Collection<Cliente> clientes = service.getAll();

        // Assert the response
        Assertions.assertEquals(2, clientes.size(), "findAll should return 2 clientes");
    }
	
	@Test
    @DisplayName("Test findById Success")
    void testFindById() {
        // Setup our mock repository
        Cliente cliente = ClienteCreator.createClienteToBeSaved();
        doReturn(Optional.of(cliente)).when(repository).findById(1);

        // Execute the service call
        Optional<Cliente> returnedCliente = service.getId(1);

        // Assert the response
        Assertions.assertTrue(returnedCliente.isPresent(), "Cliente was not found");
        Assertions.assertSame(returnedCliente.get(), cliente, "The Cliente returned was not the same as the mock");
    }
	
	@Test
    @DisplayName("Test save cliente")
    void testSave() {
        // Setup our mock repository
		Cliente cliente = ClienteCreator.createClienteToBeSaved();        		
        doReturn(cliente).when(repository).save(any());

        // Execute the service call
        Cliente returnedCliente = service.save(cliente);

        // Assert the response
        Assertions.assertNotNull(returnedCliente, "The saved cliente should not be null");        
    }
	
	
	@Test
    @DisplayName("Test update cliente")
    void testUpdate() {
        // Setup our mock repository
		Cliente cliente = ClienteCreator.createValidUpdateClienteToBeSaved();        		
        doReturn(cliente).when(repository).save(any());

        // Execute the service call
        Cliente returnedCliente = service.save(cliente);

        // Assert the response
        Assertions.assertNotNull(returnedCliente, "The saved cliente should not be null");        
    }
		
}
