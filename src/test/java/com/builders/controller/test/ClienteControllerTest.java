package com.builders.controller.test;


import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import com.builders.model.Cliente;
import com.builders.util.ClienteCreator;

import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class ClienteControllerTest {
	
	 static {
	        RestAssured.baseURI = "http://localhost:8080/cliente";
	    }
	    
	    @Test
	    @DisplayName("Test Create Cliente ")
	    public void test_createCliente() {
	        try {
	        	Cliente cliente = ClienteCreator.createCliente2ToBeSaved();
	        	
	            postCreateCliente(cliente)
	            .then()
	                .assertThat().spec(prepareResponseSpec(201));
	        }
	        catch(Exception e) {
	            fail("Error occurred while testing cliente  create endpoint", e);
	        }
	    }

	    @Test
	    @DisplayName("Test Get Clientes")
	    public void test_getClientes() {
	        try {
	            postCreateCliente(ClienteCreator.createClienteToBeSaved());
	            postCreateCliente(ClienteCreator.createClienteValid());

	            given()
	                .get("/")
	            .then()
	                .assertThat().spec(prepareResponseSpec(200))
	            .and()
	                .assertThat().body("data", is(not(empty())));
	        }
	        catch (Exception e) {
	            fail("Error occurred while testing fetch cliente s", e);
	        }
	    }

	    
	    @Test
	    @DisplayName("Test Update Cliente ")
	    public void test_updateCliente() {
	        try {
	            // Create Cliente 
	            Cliente cliente = ClienteCreator.createValidUpdateClienteToBeSaved();
	            postCreateCliente(cliente);

	            // Update cliente 
	            cliente.setNome("Updated-"+cliente.getNome());
	            cliente.setSobrenome("Updated-"+cliente.getSobrenome());

	            given()
	                .contentType("application/json")
	                .body(cliente)	             
	            .when()
	                .put("/")
	            .then()
	                .assertThat().spec(prepareResponseSpec(202));

	           
	            given()
	                .pathParam("id", cliente.getId())
	            .when()
	                .get("/{id}")
	            .then()
	                .assertThat().spec(prepareResponseSpec(200))
	            .and()
	                .assertThat().body("nome", equalTo(cliente.getNome()))
	            .and()
	                .assertThat().body("sobrenome", equalTo(cliente.getSobrenome()));
	        }
	        catch(Exception e) {
	            fail("Error occurred while testing cliente  update", e);
	        }
	    }

	    @Test
	    @DisplayName("Test Delete Cliente ")
	    public void test_deleteCliente() {
	        try {
	            // Create Cliente 
	            Cliente cliente = ClienteCreator.ClienteToBeSearch();
	            postCreateCliente(cliente);

	            // Delete Cliente 
	            given()
	                .pathParam("id", cliente.getId())
	            .when()
	                .delete("/{id}")
	            .then()
	                .assertThat().spec(prepareResponseSpec(200));

	            // Trying to get the deleted cliente  should throw 404
	            given()
	                .pathParam("id", cliente.getId())
	            .when()
	                .get("/{id}")
	            .then()
	                .assertThat().spec(prepareResponseSpec(404));
	        }
	        catch(Exception e) {
	            fail("Error occurred while testing cliente  update", e);
	        }
	    }


	    private ResponseSpecification prepareResponseSpec(int responseStatus) {
	        return new ResponseSpecBuilder()
	            .expectStatusCode(responseStatus)
	            .build();
	    }

	    private Response postCreateCliente(Cliente cliente) throws Exception {
	        RequestSpecification request
	            = given()
	                .contentType("application/json")
	                .body(cliente);

	        return request.post("/");
	    }

}